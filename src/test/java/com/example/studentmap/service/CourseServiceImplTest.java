package com.example.studentmap.service;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import java.util.ArrayList;
import java.util.List;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.example.studentmap.dao.CourseRepository;
import com.example.studentmap.model.Course;


@RunWith(MockitoJUnitRunner.Silent.class)
public class CourseServiceImplTest {

	@InjectMocks
	CourseServiceImpl courseServiceImpl;

	@Mock
	CourseRepository courseRepository;

	@Test
	public void testGetAllCoursesForPositive() {
		List<Course> courses = new ArrayList<Course>();
		Course course = new Course();
		course.setCourseId(1);
		course.setName("java");
		course.setDescription("spring");
		courses.add(course);
		Course course1 = new Course();
		course.setCourseId(2);
		course.setName("java");
		course.setDescription("spring");
		courses.add(course1);
		Mockito.when(courseRepository.findAll()).thenReturn(courses);
		List<Course> course2 = courseServiceImpl.getallCourses();
		Assert.assertNotNull(course2);
		Assert.assertEquals(2, course2.size());

	}

	@Test
	public void testGetAllCoursesForNegative() {
		List<Course> courses = new ArrayList<Course>();
		Course course = new Course();
		course.setCourseId(-1);
		course.setName("java");
		course.setDescription("spring");
		courses.add(course);
		Mockito.when(courseRepository.findAll()).thenReturn(courses);
		List<Course> course1 = courseServiceImpl.getallCourses();
		Assert.assertNotNull(course1);
		Assert.assertEquals(1, course1.size());

	}

	@Test
	public void testCreateCoursesForPositive() {
		Course course = new Course(1, "java", "spring");
		Mockito.when(courseRepository.save(course)).thenReturn(course);
		Assert.assertEquals(course, courseServiceImpl.addCourse(course));
	}

	@Test
	public void testCreateCourseForNegative() {
		Course course = new Course(-1, "java", "spring,Hibenate");
		Mockito.when(courseRepository.save(course)).thenReturn(course);
		Assert.assertEquals(course, courseServiceImpl.addCourse(course));
	}

	@Test
	public void testUpdateCoursesForPositive() {
		Course course = new Course(1, "java", "spring");
		Mockito.when(courseRepository.save(course)).thenReturn(course);
		Assert.assertEquals(course, courseServiceImpl.updateCourse(course));
	}

	@Test
	public void testUpdateCoursesForNegative() {
		Course course = new Course(-1, "java", "spring");
		Mockito.when(courseRepository.save(course)).thenReturn(course);
		Assert.assertEquals(course, courseServiceImpl.updateCourse(course));
	}

	@Test
	public void removeCoursetForPositive() {
		new Course(11, "java", "spring");
		courseServiceImpl.deleteCourse(11);
		verify(courseRepository, times(1)).deleteById((long) 11);
	}

	@Test
	public void removeCoursetForNegative() {
		new Course(-1, "java", "spring");
		courseServiceImpl.deleteCourse(-1);
		verify(courseRepository, times(1)).deleteById((long) -1);
	}

}
