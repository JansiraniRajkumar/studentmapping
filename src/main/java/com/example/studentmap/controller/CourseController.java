package com.example.studentmap.controller;

import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.studentmap.model.Course;
import com.example.studentmap.service.CourseService;


@RestController
@RequestMapping("/courses")
public class CourseController {

	@Autowired
	private CourseService courseService;

	@GetMapping("/course")
	public ResponseEntity<List<Course>> getAllCourses() {
		List<Course> courses = courseService.getallCourses();
		return new ResponseEntity<List<Course>>(courses, HttpStatus.OK);

	}

	/*
	 * @PutMapping(value = "/course/{id}/{alias}", produces =
	 * MediaType.TEXT_PLAIN_VALUE) public String updateCourse(@PathVariable("id")
	 * String name, @PathVariable("alias") Long courseId) { return
	 * courseService.updateCourse(name, courseId);
	 * 
	 * }
	 */

	@PostMapping(value = "/course")
	public ResponseEntity<Course> addCourse(@RequestBody Course course) {
		Course course1 = courseService.addCourse(course);
		return new ResponseEntity<Course>(course1, HttpStatus.OK);
	}

	@DeleteMapping(value = "/course/{id}")
	public ResponseEntity<String> deleteCourse(@PathVariable("id") long courseId) {
		courseService.deleteCourse(courseId);
		return new ResponseEntity<String>("Course deleted successfully", HttpStatus.OK);
	}

	@PutMapping(value = "/course6")
	public ResponseEntity<Course> updateCourse(@Valid @RequestBody Course course) {
		Course updated = courseService.updateCourse(course);
		return new ResponseEntity<Course>(updated, HttpStatus.OK);
	}

	/*
	 * @GetMapping(value = "/course5/{id}") public Course
	 * getCourseById(@PathVariable("id") long courseId) { return
	 * this.getCourseById(courseId); }
	 */
	
	@GetMapping(value = "/course4/{id}")
	public ResponseEntity<Course> readCourseById(@PathVariable("id") long courseId) {
		Course course=courseService.getCourseById(courseId);
		return new ResponseEntity<Course>(course, HttpStatus.OK);
	}

}
