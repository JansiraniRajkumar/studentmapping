package com.example.studentmap.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.studentmap.model.Course;


public interface CourseRepository extends JpaRepository<Course, Long> {

}
