package com.example.studentmap.service;

import java.util.List;

import com.example.studentmap.model.Course;




public interface CourseService {

	public List<Course> getallCourses();

	public Course updateCourse(Course course);

	public Course addCourse(Course course);

	public void deleteCourse(long courseId);

	public Course getCourseById(long courseId);

}
