package com.example.studentmap.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.studentmap.dao.CourseRepository;
import com.example.studentmap.model.Course;


@Service
@Transactional
public class CourseServiceImpl implements CourseService {

	@Autowired
	private CourseRepository courseRepository;

	@Override
	public List<Course> getallCourses() {
		List<Course> courses = new ArrayList<>();
		courseRepository.findAll().forEach(courses::add);
		return courses;
	}

	/*
	 * @Override public String updateCourse(String name, long courseId) { Student
	 * student = studentRepository.findByStudentName(name); Optional<Course> course
	 * = courseRepository.findById(courseId); Course courses = null; if
	 * (course.isPresent()) { courses = course.get(); } courses.setStudent(student);
	 * courseRepository.save(courses); return "success"; }
	 */

	@Override
	public Course addCourse(Course course) {
		return courseRepository.save(course);
	}

	@Override
	public void deleteCourse(long courseId) {
		courseRepository.deleteById(courseId);

	}

	@Override
	public Course updateCourse(Course course) {
		Optional<Course> courses = courseRepository.findById(course.getCourseId());

		if (courses.isPresent()) {
			Course newEntity = courses.get();
			newEntity.setCourseId(course.getCourseId());
			newEntity.setName(course.getName());
			newEntity.setDescription(course.getDescription());
			newEntity.setStudent(course.getStudent());
			newEntity = courseRepository.save(newEntity);
			return newEntity;
		} else {
			course = courseRepository.save(course);
			return course;

		}
	}
	@Override
	public Course getCourseById(long courseId) {
		return courseRepository.findById(courseId).get();
	}


}
