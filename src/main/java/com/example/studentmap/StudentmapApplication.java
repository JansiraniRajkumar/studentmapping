package com.example.studentmap;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class StudentmapApplication {

	public static void main(String[] args) {
		SpringApplication.run(StudentmapApplication.class, args);
	}

}
